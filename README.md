# libgraphique

Bibliothèque écrite en C permettant d&#39;afficher une fenêtre et des éléments graphiques simplement.

Utile pour les étudiants et ceux désireux d&#39;apprendre le C et l&#39;algorithmie facilement.