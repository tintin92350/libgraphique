/*******************************************************************************
 * 
 * LIBGRAPHIQUE 2.0
 * 
 * Ecrit par Quentin RODIC le 16 Juin 2019
 * 
 * Ce fichier contient les entêtes nécessaire au 
 * fonctionnement de la bibliothèque
 * 
 ******************************************************************************/

// Entêtes standard
#include <stdio.h>
#include <stdlib.h>

// Fichiers d'entête de la SDL
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

// Fichiers d'entête pour OpenGL
#include <GL/gl.h>
#include <GL/glu.h>

// Couleurs
#include "Couleurs.h"

// Structure de donnée pour représenter
// les polices de caractère
typedef struct {
    TTF_Font **     polices;    // Les polices stockés
    unsigned int    taille;     // La taille du tableau (combien de police)
} Polices;

// Maximum de police autorisé
#define MAXIMUM_POLICE_TABLEAU 100

// Structure de donnée
// qui contient les données de la fenetre
struct libgraphique
{
    SDL_Surface *   ecran;      // Fenêtre
    unsigned int    largeur;    // Largeur de la fenêtre
    unsigned int    hauteur;    // Hauteur de la fenêtre
    char *          titre;      // Titre de la fenêtre

    SDL_Event       evenements; // Structure d'événement

    Polices         polices;    // Tableau de police
};

typedef struct libgraphique libgraphique_t;

// Structure de dimension
typedef struct
{
    unsigned int largeur;       // Largeur supérieur ou égale à zéro
    unsigned int hauteur;       // Hauteur supérieur ou égale à zéro
} Dimension;

// Structure de position
// Attention dans une fenêtre la position (0, 0) est situé
// en haut à gauche de l'écran et les ordonnées sont inversés
typedef struct
{
    unsigned int x;            // Coordonnée en abscisse
    unsigned int y;            // Coordonnée en ordonnée
} Position;

// Structure de clique d'une souris
// elle contient la position du clique
// ainsi que le bouton qui a été pressé
typedef struct
{
    unsigned int    bouton;    // Bouton pressé
    Position        position;  // Position du clique
} BoutonSouris;


// Type Couleur
typedef Uint32 Couleur;


////////////////////////////////////////////////////////////////////////////////
// 1. Ouvrir, fermer et actualiser une session graphique

// Fonction qui démarre une session graphique
// en initialisant la fenêtre à une taille et
// un titre prédéfinis en paramètre
void demarre_session_graphique(
    const unsigned int largeur,
    const unsigned int hauteur,
          char *       titre );

// Fonction qui met un terme à la session 
// graphique qui est actuellement utilisée
void terminer_session_graphique(void);

// Fonction qui test si l'arret de la session
// a été demandé par l'utilisateur (touche échappe
// ou bouton fermer de la fenêtre)
void __test_arret(void);

// Actualise la fenêtre en observant les événements
// A utiliser sinon il n'y a pas de modification
// apparente à l'écran
void actualise(void);


////////////////////////////////////////////////////////////////////////////////
// 2. Fonctions de dessin basiques

// Fonction qui permet d'éffacer l'écran
// avec une couleur fourni
void effacer(Couleur couleur);

// Fonction de dessin basique
// permet le changement en couleur d'un pixel
void changer_pixel( Position pixel, 
                    Couleur couleur);

// Fonction de dessin basique
// permet de dessiner un rectangle de couleur
// et de taille donnés en paramètre
void dessiner_rectangle(Position    position, 
                        Dimension   dimension,
                        Couleur     couleur);

// Fonction de dessin basique
// permet de dessiner une ligne d'un point A
// à un point B d'une couleur donnée
void dessiner_ligne(Position    A,
                    Position    B,
                    Couleur     couleur);

// Fonction de dessin basique
// permet de dessiner un disque de centre O
// et de rayon R et d'une couleur donnée
void dessiner_disque(   Position        O,
                        unsigned int    R,
                        Couleur         couleur);

////////////////////////////////////////////////////////////////////////////////
// 3. Gestion des événements

// Renvoie le code SDLK de la prochaine touche pressée
// fonction bloquante
int attendre_touche(void);

// Renvoie le code SDLK de la prochaine touche pressée
// s'arrete au bout de n millisecond !
// fonction bloquante
int attendre_touche_duree(unsigned int ms);

// Renvoie les coordonnees du prochain clic (gauche ou droite) de souris
// fonction bloquante
Position attendre_clic(void);

// Renvoie une structure BoutonSouris qui détermine
// quelle bouton de la souris a été préssé et la position
// du curseur
BoutonSouris attendre_clic_gauche_droite(void);

////////////////////////////////////////////////////////////////////////////////
// 4. Affichage de texte
// pour fonctionner, la police doit se trouver dans le dossier lib
// (la police peut être changée en changeant la variable globale NOM_POLICE )

// Ajoute une police au tableau
void ajouter_police(char * cle, 
                    char * chemin_police);

// Fonction qui détermine l'identifiant d'un tableau
// en fonction d'une chaine de caractère (hashage)
int identifiant_depuis_nom(const char * cle);

// Fonction qui vérifie si une police existe
// dans le tableau grace au nom
int police_existe(char * nom);

// Fonction qui renvoie une police existe
// dans le tableau grace au nom
TTF_Font * police_nom(char * nom);

// Affiche un text à une position et une couleur
// et de taille passés en paramètre de la fonction
void afficher_text( char *       texte,
                    Position     position,
                    Couleur      couleur,
                    char *       police);


////////////////////////////////////////////////////////////////////////////////
// 5. Affichage avancé
// Fonction d'affichage avancé pour dessiner des courbes, etc
//

// Fonction qui permet de tracer le repère d'origine (0,0)
// Le repère sera donc en pixel (largeur/2, hauteur/2)
void tracer_repere(Couleur couleur);

// Fonction qui permet de dessiner la courbe
// d'une fonction passer en paramètre
void tracer_courbe( float min, 
                    float max, 
                    float (*function)(float), 
                    float pas,
                    float facteur_scale);