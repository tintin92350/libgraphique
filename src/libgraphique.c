/*******************************************************************************
 * 
 * LIBGRAPHIQUE 2.0
 * 
 * Ecrit par Quentin RODIC le 16 Juin 2019
 * 
 * Ce fichier contient les définitions des fonctions déclarées
 * dans l'entête de la bibliothèque
 * 
 ******************************************************************************/

// Libgraphique
#include "libgraphique.h"

////////////////////////////////////////////////////////////////////////////////
// Variables globales

// Libgraphique
libgraphique_t session = { NULL, 800, 600, "", {}, {NULL, 0} };

////////////////////////////////////////////////////////////////////////////////
// 1. Ouvrir, fermer et actualiser une session graphique

// Fonction qui démarre une session graphique
// en initialisant la fenêtre à une taille et
// un titre prédéfinis en paramètre
void demarre_session_graphique(
    const unsigned int largeur,
    const unsigned int hauteur,
          char *       titre )
{
    // On initialise le SDL
    SDL_Init(SDL_INIT_VIDEO);

    // On initialise TTF
    TTF_Init();

    // On renseigne les dimensions de la fenêtre
    session.largeur = largeur;
    session.hauteur = hauteur;
    session.titre   = titre;

    // On créer le fenêtre
    session.ecran = SDL_SetVideoMode(largeur, hauteur, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    // On met le nom de la fenêtre
    SDL_WM_SetCaption(titre, NULL);
}

// Fonction qui met un terme à la session 
// graphique qui est actuellement utilisée
void terminer_session_graphique(void)
{
    // On libère chaque police
    if(session.polices.taille != 0)
    {
        for (size_t i = 0; i < MAXIMUM_POLICE_TABLEAU; i++)
        {
            if(session.polices.polices[i] != NULL)
                TTF_CloseFont(session.polices.polices[i]);
        }
    }

    // On quit TTF
    TTF_Quit();

    // On quit la SDL
    SDL_Quit();
    
}

// Fonction qui test si l'arret de la session
// a été demandé par l'utilisateur (touche échappe
// ou bouton fermer de la fenêtre)
void __test_arret(void)
{
    // Doit-on quitter la session ?
    // Touche échappe ou le bouton fermer de la fenêtre
    if ((session.evenements.type == SDL_QUIT) || 
            ((session.evenements.type == SDL_KEYDOWN &&(session.evenements.key.keysym.sym == SDLK_ESCAPE)) 
       ))
       {
           terminer_session_graphique();
       }
}

// Actualise la fenêtre en observant les événements
// A utiliser sinon il n'y a pas de modification
// apparente à l'écran
void actualise(void)
{
    /*
     * EXERCISE:
     * Replace this with a call to glFrustum.
     */
    // On gère les événements
    SDL_PollEvent(&session.evenements);

    // On test l'arret de la session
    __test_arret();

    // On met à jour l'écran
    SDL_Flip(session.ecran);
}

////////////////////////////////////////////////////////////////////////////////
// 2. Fonctions de dessin basiques

// Fonction qui permet d'éffacer l'écran
// avec une couleur fourni
void effacer(Couleur couleur)
{
    dessiner_rectangle((Position){0, 0}, (Dimension){session.largeur, session.hauteur}, couleur);
}

// Fonction de dessin basique
// permet le changement en couleur d'un pixel
void changer_pixel( Position pixel, 
                    Couleur couleur)
{
    if( pixel.x >= 0 && pixel.x < session.largeur &&
        pixel.y >= 0 && pixel.y < session.hauteur)
        *( (Uint32*)session.ecran->pixels + pixel.y * session.largeur + pixel.x ) = couleur ;        
}

// Fonction de dessin basique
// permet de dessiner un rectangle de couleur
// et de taille donnés en paramètre
void dessiner_rectangle(Position    position, 
                        Dimension   dimension,
                        Couleur     couleur)
{
    SDL_Rect dst = { position.x, position.y, dimension.largeur, dimension.hauteur };
    SDL_FillRect(session.ecran, &dst, couleur);
}

// Fonction de dessin basique
// permet de dessiner une ligne d'un point A
// à un point B d'une couleur donnée
void dessiner_ligne(Position    A,
                    Position    B,
                    Couleur     couleur)
{
    int dx = abs(B.x-A.x), sx = A.x<B.x ? 1 : -1;
    int dy = abs(B.y-A.y), sy = A.y<B.y ? 1 : -1; 
    int err = (dx>dy ? dx : -dy)/2, e2;

    for(;;){
        changer_pixel(A, couleur);
        if (A.x==B.x && A.y==B.y) break;
        e2 = err;
        if (e2 >-dx) { err -= dy; A.x += sx; }
        if (e2 < dy) { err += dx; A.y += sy; }
    }
}

// Fonction de dessin basique
// permet de dessiner un disque de centre O
// et de rayon R
void dessiner_disque(   Position        O,
                        unsigned int    R,
                        Couleur         couleur)
{
    for (size_t x = O.x-R; x < O.x+R; x++)
    {
        for (size_t y = O.y-R; y < O.y+R; y++)
        {
            Position point = { x, y };
            float distance = (O.x-point.x)*(O.x-point.x) + (O.y-point.y)*(O.y-point.y);

            if(distance <= R*R)
                changer_pixel(point, couleur);
        }
    }
    
}

////////////////////////////////////////////////////////////////////////////////
// 3. Gestion des événements

// Renvoie le code SDLK de la prochaine touche pressée
// fonction bloquante
int attendre_touche(void)
{
    do {
        SDL_WaitEvent(&session.evenements) ;
        __test_arret() ;
    }
    while (session.evenements.type != SDL_KEYDOWN ) ;
    
    return session.evenements.key.keysym.sym;
}

// Renvoie le code SDLK de la prochaine touche pressée
// s'arrete au bout de n millisecond !
// fonction bloquante
int attendre_touche_duree(unsigned int ms)
{
    Uint32 depart = SDL_GetTicks();
    Uint32 courant = SDL_GetTicks();

    do {
        SDL_PollEvent(&session.evenements) ;
        __test_arret() ;

        courant = SDL_GetTicks();
    }
    while (session.evenements.type != SDL_KEYDOWN && courant - depart < ms) ;
    
    return session.evenements.key.keysym.sym;
}

// Renvoie les coordonnees du prochain clic (gauche ou droite) de souris
// fonction bloquante
Position attendre_clic(void)
{
    do {
        SDL_WaitEvent(&session.evenements) ;
        __test_arret();
    }
    while (session.evenements.type != SDL_MOUSEBUTTONDOWN) ;

    Position p ;
    p.x = session.evenements.button.x ;
    p.y = session.evenements.button.y ;

    return p;
}

// Renvoie une structure BoutonSouris qui détermine
// quelle bouton de la souris a été préssé et la position
// du curseur
BoutonSouris attendre_clic_gauche_droite(void)
{
    do {
        SDL_WaitEvent(&session.evenements) ;
        __test_arret();
    }
    while (session.evenements.type != SDL_MOUSEBUTTONDOWN) ;

    Position p ;
    p.x = session.evenements.button.x ;
    p.y = session.evenements.button.y ;

    unsigned int bouton = session.evenements.button.button;

    return (BoutonSouris) { bouton, p }; 
}

////////////////////////////////////////////////////////////////////////////////
// 4. Affichage de texte
// pour fonctionner, la police doit se trouver dans le dossier lib
// (la police peut être changée en changeant la variable globale NOM_POLICE )

// Ajoute une police au tableau
void ajouter_police(char * cle, 
                    char * chemin_police)
{
    if(session.polices.polices == NULL)
        session.polices.polices = calloc(MAXIMUM_POLICE_TABLEAU, sizeof(TTF_Font*));

    session.polices.polices[identifiant_depuis_nom(cle)] = TTF_OpenFont(chemin_police, 10);
}

// Fonction qui détermine l'identifiant d'un tableau
// en fonction d'une chaine de caractère (hashage)
int identifiant_depuis_nom(const char * cle)
{
    int r = 0;
    int len = strlen(cle);

    for (size_t i = 0; i < len; i++)
        r += (int)cle[i] * i;

    return r % MAXIMUM_POLICE_TABLEAU;    
}

// Fonction qui vérifie si une police existe
// dans le tableau grace au nom
int police_existe(char * nom)
{
    int id = identifiant_depuis_nom(nom);

    return session.polices.polices[id] != NULL;
}

// Fonction qui renvoie une police existe
// dans le tableau grace au nom
TTF_Font * police_nom(char * nom)
{
    return session.polices.polices[identifiant_depuis_nom(nom)];
}

// Affiche un text à une position et une couleur
// et de taille passés en paramètre de la fonction
void afficher_text( char *       texte,
                    Position     position,
                    Couleur      couleur,
                    char *       police)
{
    if(police_existe(police) == 1)
    {
        TTF_Font * police_ttf = police_nom(police);

        SDL_Color coul_police ;
        SDL_GetRGB(couleur, session.ecran->format,&(coul_police.r),&(coul_police.g),&(coul_police.b));

        SDL_Surface *surftexte= TTF_RenderText_Blended(police_ttf, texte, coul_police);
        SDL_Rect rect;

        rect.x = position.x;
        rect.y=  position.y;
        SDL_BlitSurface(surftexte, NULL, session.ecran, &rect);
    }
}

////////////////////////////////////////////////////////////////////////////////
// 5. Affichage avancé
// Fonction d'affichage avancé pour dessiner des courbes, etc
//

// Fonction qui permet de tracer le repère d'origine (0,0)
// Le repère sera donc en pixel (largeur/2, hauteur/2)
void tracer_repere(Couleur couleur)
{
    dessiner_ligne((Position){0, session.hauteur/2}, (Position){session.largeur,session.hauteur/2}, couleur);

    dessiner_ligne((Position){session.largeur/2, 0}, (Position){session.largeur/2,session.hauteur}, couleur);
}

// Fonction qui permet de dessiner la courbe
// d'une fonction passer en paramètre
void tracer_courbe( float min, 
                    float max, 
                    float (*function)(float), 
                    float pas,
                    float facteur_scale)
{
    for(float x = min; x <= max; x+=pas)
        changer_pixel((Position){(x*facteur_scale)+session.largeur/2, session.hauteur/2 - function(x)*facteur_scale}, red);
}