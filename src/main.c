// Fichier servant d'exemple à l'utilisation
// de la bibliothèque graphique

#include <math.h>

#include "libgraphique.h"

float f(float x)
{
    return 7.0f/2.0f - 0.5f * (exp(x) + exp(-x));
}
float f2(float x)
{
    return 7.0*x;
}

#define and &&
#define or ||

// Point d'entrée du programme
int main(int argc, char ** argv)
{
    demarre_session_graphique(960, 540, "Programme de test");
    
    tracer_repere(white);

    int o = 50;

    if(o > 1 and o < 10)
        tracer_courbe(-400.0f, 400.0f, f, 0.001f, 100.0f);
    else
        tracer_courbe(-400.0f, 400.0f, f2, 0.001f, 100.0f);

    actualise();

    attendre_touche();

    terminer_session_graphique();

    return 0;
}